# Getting Web Data Commons NQuads and Just the Small Set of Interest

[Web Data Commons](http://webdatacommons.org/) (WDC) makes its files of NQuads easy to grab. Below are the basic steps I took. I did this work over the course of some weeks and am recreating these steps from my notes and redoing some of it, so there might be some issues. I'll improve these steps over time with feedback.

## wget

I had an issue with the line endings not working correctly on my Linux system so I had to do the following. First I grabbed the list of URLs for all the files. To make this easy to follow along we'll start in our home directory.

```bash
cd ~
mkdir webdatacommons
cd webdatacommons
wget http://webdatacommons.org/downloads/2012-08/nquads/files.list
```

Then I used vim to correct the line endings. You may try other tools like dos2unix, though that didn't work for me.

```
vim files.list
:%s/\r/\r/g
```

Next I used wget to download the list of files and put them in the data directory:

```
nohup wget -i files.list -P data
```

Now wait until it all downloads.

## Split by embedded semantic markup type

Next we can gunzip each of the files of NQuads and separate the files of each of the types of embedded semantic markup into individual folders. This will allow us to do analysis based on the markup type. Much of this could have been scripted, but I ended up doing it rather manually.

Gunzip all the files.

```
cd data
gunzip *gz
```

Move them into individual folders. Here is an example of what I did:

```
mkdir rdfa
mv html-rdfa* rdfa/.
mkdir microdata
mv html-microdata* microdata/.
```

I repeated this for each of the types that I was interested in. With the Microformats sets you may choose to group them all together or separate them more granularly. 

Then go into each of these directories and reduce the set with grep for terms or text of interest. In this case I'm interested in NQuads with the presence of ".edu" in them. This is a loose indicator of educational institutions. Later we'll use some scripts in this gem to pull out where in the quad ".edu" is found. Note that in the `grep` command the "." needs to be escaped with a backslash to match a literal period. We'll `cat` all the files of a type together and output the matching quads into a single file in a new directory.

```
mkdir ~/webdatacommons/dot_edu
cd microdata
cat html* | grep "\.edu" > ~/webdatacommons/dot_edu/microdata_dot_edu.nq
```

Repeat for each embedded semantic markup type directory you created above. Give the output file a good name to distinguish them.

## The dot_edu set

To save you the effort of creating the dot_edu set yourself, you can grab it here:

<https://docs.google.com/file/d/0B8SS5OUXWs4Gc3JROVl0MF83cGM/edit?usp=sharing>

I release this data under the same terms as the Common Crawl Corpus and Web Data Commons. The data is provided according the same [terms of use, disclaimer of warranties and limitation of liabilities](http://commoncrawl.org/about/terms-of-use/full-terms-of-use/) that apply to the Common Crawl corpus.

## Extracting data from the set

Now you can use the scripts included with the austin gem to parse the data into CSV for analysis.