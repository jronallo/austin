module Austin
  class Extractor

    def initialize(statement)
      raise if !statement.is_a? RDF::Statement
      @statement = statement
    end

    # Subject stuff

    # The subject as a string. If the subject is a node (a blank node), then
    # do not save the string, but just an underscore.
    def subject_string
      if @statement.subject.node?
        '_'
      else
        @statement.subject.to_s
      end
    end

    # The host of the subject URI if the subject is a URI.
    def subject_host
      uri_host @statement.subject
    end

    # boolean whether the subject is a URI
    def subject_uri?
      @statement.subject.uri?
    end

    def subject_pld
      pld @statement.subject
    end

    # boolean whether the subject contains ".edu"
    def subject_dot_edu?
      dot_edu? @statement.subject
    end

    # boolean whether the subject contains some indication it is from a library
    def subject_library?
      library? @statement.subject
    end

    # boolean whether subject is a blank node
    def subject_blank_node?
      @statement.subject.node?
    end

    # Predicate stuff

    # predicate as a string
    def predicate_string
      @statement.predicate.to_s
    end

    # host of the predicate
    def predicate_host
      @statement.predicate.host
    end

    def predicate_pld
      pld @statement.predicate
    end

    # boolean whether the predicate is .edu
    def predicate_dot_edu?
      dot_edu? @statement.predicate
    end

    # boolean whether the predicate is from a library
    def predicate_library?
      library? @statement.predicate
    end

    def schemaorg?
      pred = @statement.predicate.to_s
      if pred.include?('http://schema.org/')
        true
      else
        false
      end
    end

    def schemaorg_type
      pred = @statement.predicate.to_s
      if pred.include?('http://schema.org/')
        pred.split('/')[-2]
      end
    end

    def schemaorg_property
      pred = @statement.predicate.to_s
      if pred.include?('http://schema.org/')
        pred.split('/').last
      end
    end

    #object stuff

    def object_uri?
      @statement.object.uri?
    end

    def object_string
      if @statement.object.uri?
        @statement.object.to_s
      end
    end

    def object_pld
      pld @statement.object
    end

    def object_dot_edu?
      dot_edu?(@statement.object)
    end

    def object_library?
      library? @statement.object
    end

    # context stuff

    def context_string
      @statement.context.to_s
    end

    def context_host
      @statement.context.host if @statement.context
    end

    def context_dot_edu?
      dot_edu? @statement.context
    end

    def context_library?
      library? @statement.context
    end

    def context_pld
      pld @statement.context
    end

    # for CSV

    def self.csv_columns
      [:subject_string, :subject_host, :subject_pld, :subject_uri?, :subject_dot_edu?,
        :subject_library?, :subject_blank_node?,
        :predicate_string, :predicate_host, :predicate_pld, :predicate_dot_edu?, :predicate_library?,
        :schemaorg?, :schemaorg_type, :schemaorg_property,
        :object_uri?, :object_string, :object_pld, :object_dot_edu?, :object_library?,
        :context_string, :context_host, :context_pld, :context_dot_edu?, :context_library?]
    end

    def csv_row
      row = []
      Extractor.csv_columns.each do |column|
        row << send(column)
      end
      row
    end

    private

    def dot_edu?(component)
      if component && component.uri?
        if %r{\.edu(/|$|\.\w\w)}.match component.host
          true
        else
          false
        end
      else
        false
      end
    end

    def library?(component)
      if component && component.uri? && dot_edu?(component)
        if %r{(lib\.|library\.|libraries\.)}.match component.host
          true
        else
          false
        end
      else
        false
      end
    end

    def uri_host(component)
      component.host if component.uri?
    end

    def pld(component)
      if component
        component_host = uri_host(component)
        if component_host
          parts = component_host.split('.')
          if /^\w\w$/.match parts.last
            parts.last(3).join('.')
          else
            parts.last(2).join('.')
          end
        end
      end
    end

  end
end