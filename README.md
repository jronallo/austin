# Austin

A gem for extracting data out of a subset of the Web Data Commons corpus of NQuads. Mainly useful for looking at nquads that contain ".edu" somewhere within. A similar approach could be used to investigate other contexts of interest. Results in a CSV file where each of the components of the NQuad have various facets created for them.

## Installation

This code has not been released as a gem yet, so clone the repository:

```bash
$ git clone https://jronallo@bitbucket.org/jronallo/austin.git
```

And then execute:

```bash
$ cd austin
$ bundle
```

## Prerequisites

There are some steps that lead up to the point where this gem is useful. I've written some documentation in the docs directory here for more on that. Before using this tool you must already have some NQuads that you're interested in analyzing.

## Assumptions

For these scripts to work you must have a structure to your files similar to the below. Filenames must end in "nq". Filenames are split on an underscore to determine the semantic embedded markup type for the "type_f" column in the resulting CSV file.

```
dot_edu
├── mf-geo_dot_edu.nq
├── mf-hcalendar_dot_edu.nq
├── mf-hcard_dot_edu.nq
├── mf-hlisting_dot_edu.nq
├── mf-hrecipe_dot_edu.nq
├── mf-hresume_dot_edu.nq
├── mf-hreview_dot_edu.nq
├── mf-species_dot_edu.nq
├── mf-xfn_dot_edu.nq
├── microdata_dot_edu.nq
└── rdfa_dot_edu.nq
```

See doc/getting\_webdatacommons_nquads.md for more information on how to get your data into this state. Or skip to the bottom of that page for a download of the data set.

## Usage

`austin_extract_nq.rb ~/webdatacommons/dot_edu ~/webdatacommons/dot_edu.csv`

The only thing that will be output to the screen are all the triples which could not be parsed for a variety of reasons. In many cases a URI in the quad cannot be parsed correctly. In many cases mailto links caused it to fail parsing.

You can run the following to see that it is working and that CSV rows are being created:

```
tail -f ~/webdatacommons/dot_edu.csv
```

You can currently find the resulting CSV for the August 2012 Common Crawl corpus for all NQuads containing ".edu" here:
<https://drive.google.com/file/d/0B8SS5OUXWs4GTW12clh4QTRfSzg/edit?usp=sharing>

### CSV Columns

Most facets are named after whether they were extracted from the subject, predicate, object, or context of the NQuad. The schema.org facets all come from the predicate.

TODO: Insert an explanation of what each of the columns in the CSV file mean. For now simply view the source at lib/austin/extractor.rb for how the data is calculated. Any ".edu" URIs must be at the end of the URI string or followed by a two letter country code (ccTLD). Libraries are identified only in the case that they include ".edu", as before, and also include "lib.", "library.", or "libraries.". This is rather crude but good enough for getting some sense of the prevelance of this markup in and about education institutions and their libraries.


## Splitting the CSV file

Solr importing of CSV is very fast, but it can run into some size limitations (memory limitations?) when trying to import very large files. The included split\_extracted_csv.rb script helps to split the resulting CSV file. Use it like this:

```
split_extracted_csv.rb ~/webdatacommons/dot_edu.csv
```

Look for the output in the extracted_split directory.

## Solr & CSV

Add documents:
```
curl http://localhost:8983/solr/update/csv --data-binary @extracted.csv -H 'Content-type:text/plain; charset=utf-8'
```

If you've used the `split_extracted_csv.rb` script, then you can use `index_split_extracted.rb` to index all of the CSV files.

```
index_split_extracted.rb ~/webdatacommons/extracted_split
```

Delete all documents:
```
curl http://localhost:8983/solr/update -H "Content-Type: text/xml" --data-binary '<delete><query>*:*</query></delete>'
```

## Author

Jason Ronallo

## License

MIT License