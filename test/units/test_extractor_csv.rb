require_relative '../test_helper'

class ExtractorCSVTest < ActiveSupport::TestCase

  setup do
    @extractor = common_extractor_example
  end

  test "should create CSV row" do
    expected_row = [
        "http://lib.example.edu/thing", "lib.example.edu", "example.edu", true, true, true, false, #subject
        "http://schema.org/Article/name", "schema.org", "schema.org", false, false, #predicate
        true, "Article", "name",
        false, nil, nil, false, false, #object
        "http://d.lib.ncsu.edu/collections/catalog/0004817", "d.lib.ncsu.edu", "ncsu.edu", true, true] #context
    actual_row = @extractor.csv_row
    assert_equal expected_row, actual_row
  end

end