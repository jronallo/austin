require_relative '../test_helper'

class ExtractorObjectTest < ActiveSupport::TestCase

  setup do
    @extractor = common_extractor_example
    @ex_string = %Q|_:node9b7f691 <http://vocab.example.com/thing> <http://lib.example.edu/something> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
  end

  test "should not be a URI" do
    assert_false @extractor.object_uri?
  end

  test "should be a URI" do
    ex = extractor_from_string @ex_string
    assert ex.object_uri?
  end

  test "should have object uri" do
    ex = extractor_from_string @ex_string
    assert_equal "http://lib.example.edu/something", ex.object_string
  end

  test "should not have object uri" do
    assert_nil @extractor.object_string
  end

  test "should have a dot edu" do
    ex = extractor_from_string @ex_string
    assert ex.object_dot_edu?
  end

  test "should not have a dot edu for dot com" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://vocab.example.com/thing> <http://lib.example.com/something> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_false ex.object_dot_edu?
  end

  test "should not have a dot edu for a string" do
    assert_false @extractor.object_dot_edu?
  end

  test "should be a library" do
    ex = extractor_from_string @ex_string
    assert ex.object_library?
  end

  test "should not be a library for dot com" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://vocab.example.com/thing> <http://lib.example.com/something> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_false ex.object_library?
  end

  test "should not be a library for a string" do
    assert_false @extractor.object_library?
  end

  test "should have the PLD" do
    ex = extractor_from_string @ex_string
    assert "example.edu", ex.object_pld
  end

end