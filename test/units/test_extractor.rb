require_relative '../test_helper'

class ExtractorTest < ActiveSupport::TestCase

  setup do
    @extractor = common_extractor_example
  end

  test "should raise an error if given something other than a statement" do
    # FIXME: we ought to have our own error class raised
    assert_raise RuntimeError do
      Austin::Extractor.new('a string')
    end
  end

  test "should not create a new extractor without an RDF statement" do
    assert_raise ArgumentError do
     Austin::Extractor.new
    end
  end

end