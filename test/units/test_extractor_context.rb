require_relative '../test_helper'

class ExtractorContextTest < ActiveSupport::TestCase

  setup do
    @extractor = common_extractor_example
  end

  test "should have the context string" do
    assert_equal "http://d.lib.ncsu.edu/collections/catalog/0004817", @extractor.context_string
  end

  test "should have dot edu context" do
    assert @extractor.context_dot_edu?
  end

  test "should be a library" do
    assert @extractor.context_library?
  end

  test "should have a host" do
    assert_equal "d.lib.ncsu.edu", @extractor.context_host
  end

  test "should say that an edu before a ccTLD is dot_edu" do
    statement = %Q|<http://lib.example.edu/thing> <http://schema.org/Article/name> "A Great Start" <http://digital.library.adelaide.edu.au/dspace/handle/2440/45383>   .|
    ex = extractor_from_string(statement)
    assert ex.context_dot_edu?
  end

  test "should say that library is in an ccTLD dot_edu" do
    statement = %Q|<http://lib.example.edu/thing> <http://schema.org/Article/name> "A Great Start" <http://digital.library.adelaide.edu.au/dspace/handle/2440/45383>   .|
    ex = extractor_from_string(statement)
    assert ex.context_library?
  end

  test "should have the correct PLD" do
    statement = %Q|<http://lib.example.edu/thing> <http://schema.org/Article/name> "A Great Start" <http://d.lib.ncsu.edu/collections/catalog/segIns_004>   .|
    ex = extractor_from_string(statement)
    assert_equal "ncsu.edu", ex.context_pld
  end

  test "should have the correct PLD for ccTLD" do
    statement = %Q|<http://lib.example.edu/thing> <http://schema.org/Article/name> "A Great Start" <http://digital.library.adelaide.edu.au/dspace/handle/2440/45383>   .|
    ex = extractor_from_string(statement)
    assert_equal "adelaide.edu.au", ex.context_pld
  end

end