require_relative '../test_helper'

class ExtractorSubjectTest < ActiveSupport::TestCase

  setup do
    @extractor = common_extractor_example
  end

  test "should have a subject_string" do
    assert_equal "http://lib.example.edu/thing", @extractor.subject_string
  end

  test "should have a subject host" do
    assert_equal "lib.example.edu", @extractor.subject_host
  end

  test "should have an edu subject" do
    assert @extractor.subject_dot_edu?
  end

  test "should not have an edu subject" do
    ex = extractor_from_string(%Q|<http://lib.example.com/thing> <http://schema.org/Article/name> "A Great Start" <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|)
    assert_false ex.subject_dot_edu?
  end

  test "should have a library subject" do
    assert @extractor.subject_library?
  end

  test "should not have a library subject" do
    ex = extractor_from_string %Q|_:node9b7f691f1eb9d3ad4f9c1c39c5a89e64 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Article> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_false ex.subject_library?
  end

  test "should not have a subject blank node" do
    assert_false @extractor.subject_blank_node?
  end

  test "should have a subject blank node" do
    ex = extractor_from_string %Q|_:node9b7f691f1eb9d3ad4f9c1c39c5a89e64 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Article> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert ex.subject_blank_node?
  end

  test "should have the PLD for the subject" do
    ex = extractor_from_string(%Q|<http://lib.example.com/thing> <http://schema.org/Article/name> "A Great Start" <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|)
    assert_equal "example.com", ex.subject_pld
  end

  test "should have the PLD for the subject for a ccTLD" do
    ex = extractor_from_string(%Q|<http://lib.example.com.su/thing> <http://schema.org/Article/name> "A Great Start" <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|)
    assert_equal "example.com.su", ex.subject_pld
  end

end