require_relative '../test_helper'

class ExtractorPredicateTest < ActiveSupport::TestCase

  setup do
    @extractor = common_extractor_example
  end

  test "should have the predicate as a string" do
    assert "http://schema.org/Article/name", @extractor.predicate_string
  end

  test "should not have an edu predicate" do
    assert_false @extractor.predicate_dot_edu?
  end

  test "should have an edu predicate" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://lib.example.edu/thing#type> <http://schema.org/Article> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert ex.predicate_dot_edu?
  end

  test "should not have library predicate" do
    assert_false @extractor.predicate_library?
  end

  test "should have a library predicate" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://lib.example.edu/thing#type> <http://schema.org/Article> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert ex.predicate_library?
  end

  test "should have the host" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://lib.example.edu/thing#type> <http://schema.org/Article> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_equal "lib.example.edu", ex.predicate_host
  end

  test "should have the pld" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://lib.example.edu/thing#type> <http://schema.org/Article> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_equal "example.edu", ex.predicate_pld
  end

  test "should have the pld for ccTLD" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://lib.example.edu.au/thing#type> <http://schema.org/Article> <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_equal "example.edu.au", ex.predicate_pld
  end

  test "should show whether the predicate uses schema.org" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://schema.org/Article/name> "Some title" <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_equal true, ex.schemaorg?
  end

  test "should have the schema.org type" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://schema.org/Article/name> "Some title" <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_equal "Article", ex.schemaorg_type
  end

  test "should have the schema.org property" do
    ex = extractor_from_string %Q|_:node9b7f691 <http://schema.org/Article/name> "Some title" <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    assert_equal "name", ex.schemaorg_property
  end

end