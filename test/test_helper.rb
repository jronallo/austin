require 'test/unit'
require 'austin'
require "active_support"
require 'pry'

class ActiveSupport::TestCase
  def extractor_from_string(string)
    reader = RDF::NQuads::Reader.new(string)
    statement = reader.first
    Austin::Extractor.new(statement)
  end

  def common_extractor_example
    statement = %Q|<http://lib.example.edu/thing> <http://schema.org/Article/name> "A Great Start" <http://d.lib.ncsu.edu/collections/catalog/0004817>   .|
    extractor_from_string(statement)
  end
end