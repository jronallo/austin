#!/usr/bin/env ruby

# extract_nq.rb
# script to go through a data directory of NQuads and export information about them as CSV

# bundle exec bin/extract_nq.rb path/to/nquad_directory /path/to/output/directory/file.csv

$LOAD_PATH.unshift File.join(File.dirname(__FILE__), '..', 'lib')
require 'pp'
require 'pry'
require 'securerandom'
require 'austin'

# Add "_f" to the end of column names so that they can be recognized as
# Solr dynamic fields of the same type.
def facetize_symbols(symbols)
  symbols.map do |symbol|
    symbol_name = symbol.to_s.sub('?', '')
    symbol_name + '_f'
  end
end

# We do some monkeypatching to make rdf-ruby more forgiving.
module RDF::NTriples
  class Reader
    def read_uriref(options = {})
      if uri_str = match(URIREF)
        uri_str = self.class.unescape(uri_str)
        uri = RDF::URI.send(intern? && options[:intern] ? :intern : :new, uri_str)
        uri.validate!     if validate?
        raise RDF::ReaderError, "uri not absolute" if validate? && !uri.absolute?
        uri.canonicalize! if canonicalize?
        uri
      end
    rescue ArgumentError => e
      puts uri_str
      raise RDF::ReaderError, "invalid URI"
    end
  end
end
module RDF
  class Reader
    def each_statement(&block)
      if block_given?
        begin
          loop {
            begin
              block.call(read_statement)
            rescue RDF::ReaderError
              puts @line
              next
            end
          }
        rescue EOFError => e
          rewind rescue nil
        end
      end
      enum_for(:each_statement)
    end
  end
end
# end of monkeypatching

data_directory = File.expand_path(ARGV[0])
output_file = File.expand_path(ARGV[1])

CSV.open(output_file, "wb") do |csv|
  facets = facetize_symbols Austin::Extractor.csv_columns
  csv << ["id", "type_f", facets].flatten

  # Process all of the files that end with "nq"
  Dir.glob(File.join(data_directory, '*nq')).sort.each do |filepath|
    puts filepath
    basename = File.basename(filepath)
    kind = basename.split("_").first
    RDF::NQuads::Reader.open(filepath) do |reader|
      reader.each_statement do |statement|
        begin
          nquad_extractor = Austin::Extractor.new(statement)
          nquad_csv_row = nquad_extractor.csv_row
          identifier = SecureRandom.hex(32)
          csv << [identifier, kind, nquad_csv_row].flatten
        rescue => e
          puts e.backtrace
          puts statement.inspect; exit
        end
      end
    end
  end

end #close CSV

