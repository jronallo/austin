#!/usr/bin/env ruby

# get 1000 lines at the head and tail of each file and output a new file into a sample directory

require 'fileutils'

sample_size = ARGV[0] || (puts "Must provide sample size."; exit)
sample_directory = File.expand_path('.','sample')
if !File.exist? sample_directory
  FileUtils.mkdir(sample_directory)
end

Dir.glob('*.nq').each do |filepath|
  puts filepath
  # get the main part of the filename for sample filename
  kind = File.basename(filepath).split('_').first
  new_filename = File.join(sample_directory, kind + '_dot_edu-sample.nq')
  File.open(new_filename, 'w') do |fh|
    `head -n #{sample_size} #{filepath} >> #{new_filename}`
    `tail -n #{sample_size} #{filepath} >> #{new_filename}`
  end
end
