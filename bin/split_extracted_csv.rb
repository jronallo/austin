#!/usr/bin/env ruby

# split_extracted_csv.rb
require 'tempfile'

csv_file = File.expand_path ARGV[0]
dir = File.dirname csv_file
Dir.chdir(dir)

puts 'splitting...'
`split -l 100000 #{csv_file} extracted_split_`

puts 'renaming...'
`rename 's/(.*)/$1.csv/' extracted_split_*`

puts 'moving to new directory...'
Dir.mkdir 'extracted_split'
`mv extracted_split_* extracted_split/.`

# now prepend the header to each file except the first
puts 'prepending CSV header to each file...'
files = Dir.glob('extracted_split/*').sort
first_file = files.shift
header = ''
File.open(first_file) {|f| header = f.readline}
files.each do |file|
  tempfile = Tempfile.new('csv_tempfile')
  tempfile.write header
  File.open(file, 'r') do |orig|
    tempfile.write orig.read
  end
  FileUtils.mv tempfile.path, file
end