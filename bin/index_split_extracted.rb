#!/usr/bin/env ruby

# index_split_extracted.rb /full/path/to/extracted_split/

directory = File.expand_path ARGV[0]

Dir.glob(File.join(directory, '*.csv')).sort.each do |file|
  puts "Indexing #{file}"
  `curl http://localhost:8983/solr/update/csv --data-binary @#{file} -H 'Content-type:text/plain; charset=utf-8'`
  puts "commit changes..."
  `curl 'http://localhost:8983/solr/update?commit=true'`
end

puts "commit changes..."
`curl 'http://localhost:8983/solr/update?commit=true'`
puts "optimize index..."
`curl 'http://localhost:8983/solr/update?optimize=true'`